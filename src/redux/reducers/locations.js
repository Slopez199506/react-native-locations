import {
  FETCH_LOCATIONS_BEGIN,
  FETCH_LOCATIONS_SUCCESS,
  FETCH_LOCATIONS_ERROR,
  FETCH_INSERT_LOCATIONS_BEGIN,
  FETCH_INSERT_LOCATIONS_SUCCESS,
  FETCH_INSERT_LOCATIONS_ERROR,
  FETCH_UPDATE_LOCATIONS_BEGIN,
  FETCH_UPDATE_LOCATIONS_SUCCESS,
  FETCH_UPDATE_LOCATIONS_ERROR,
  FETCH_DELETE_LOCATIONS_BEGIN,
  FETCH_DELETE_LOCATIONS_SUCCESS,
  FETCH_DELETE_LOCATIONS_ERROR,
  ONCHANGE_ADDRESS,
  ONCHANGE_DESCRIPTION,
  SELECT_LOCATION
} from './../actions/index';

const initialState = {
  loadingLocations: false,
  locations: [
    {
      "id": 1,
      "address": "colima",
      "description": "trabajo"
    },
    {
      "id": 2,
      "address": "villa de alvarez",
      "description": "casa"
    },
    {
      "id": 3,
      "address": "manzanillo",
      "description": "novia"
    }
  ],
  errorLocations: null,
  address:null,
  description:null,
  location:null
}

export const LocationReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LOCATIONS_BEGIN:
      return {
        ...state,
        loadingLocations: true,
        errorLocations: null
      }
    case FETCH_LOCATIONS_SUCCESS:
      return {
        ...state,
        loadingLocations: false,
        locations: action.locations
      }
    case FETCH_LOCATIONS_ERROR:
      return {
        ...state,
        loadingLocations: false,
        errorLocations: action.error
      }
    case FETCH_INSERT_LOCATIONS_BEGIN:
      return {
        ...state,
        loadingLocations: true,
        errorLocations: null
      }
    case FETCH_INSERT_LOCATIONS_SUCCESS:
      return {
        ...state,
        loadingLocations: false,
        locations: [...state.locations,action.locations]
      }
    case FETCH_INSERT_LOCATIONS_ERROR:
      return {
        ...state,
        loadingLocations: false,
        errorLocations: action.error
      }
    case FETCH_UPDATE_LOCATIONS_BEGIN:
      return {
        ...state,
        loadingLocations: true,
        errorLocations: null
      }
    case FETCH_UPDATE_LOCATIONS_SUCCESS:{

      return {
        ...state,
        loadingLocations: false,
        locations: [...state.locations.filter((item) => item.id !== action.location.id),action.location]
      }}
    case FETCH_UPDATE_LOCATIONS_ERROR:
      return {
        ...state,
        loadingLocations: false,
        errorLocations: action.error
      }
    case FETCH_DELETE_LOCATIONS_BEGIN:
      return {
        ...state,
        loadingLocations: true,
        errorLocations: null
      }
    case FETCH_DELETE_LOCATIONS_SUCCESS:
      return {
        ...state,
        loadingLocations: false,
        locations: [...state.locations.filter((item) => item.id !== action.location.id)]
      }
    case FETCH_DELETE_LOCATIONS_ERROR:
      return {
        ...state,
        loadingLocations: false,
        errorLocations: action.error
      }
    case ONCHANGE_ADDRESS:
      return {
        ...state,
        location: {
          ...state.location,
          address: action.address
        }
      }
    case ONCHANGE_DESCRIPTION:
      return {
        ...state,
        location: {
          ...state.location,
          description: action.description
        }
      }
    case SELECT_LOCATION:
      return {
        ...state,
        location: [...state.locations.filter((item) => item.id == action.id)][0]
      }
    default:
      return state
  }
}

export default LocationReducer;
