import {combineReducers} from 'redux';
import LocationReducer from './locations';

export const rootReducer = combineReducers({
  LocationReducer
})

export default rootReducer
