import {
  FETCH_LOCATIONS_BEGIN,
  FETCH_LOCATIONS_SUCCESS,
  FETCH_LOCATIONS_ERROR,
  FETCH_INSERT_LOCATIONS_BEGIN,
  FETCH_INSERT_LOCATIONS_SUCCESS,
  FETCH_INSERT_LOCATIONS_ERROR,
  FETCH_UPDATE_LOCATIONS_BEGIN,
  FETCH_UPDATE_LOCATIONS_SUCCESS,
  FETCH_UPDATE_LOCATIONS_ERROR,
  FETCH_DELETE_LOCATIONS_BEGIN,
  FETCH_DELETE_LOCATIONS_SUCCESS,
  FETCH_DELETE_LOCATIONS_ERROR,
  ONCHANGE_ADDRESS,
  ONCHANGE_DESCRIPTION,
  SELECT_LOCATION
} from './index';

export const onChangeAddress = (address) => ({
  type: ONCHANGE_ADDRESS,
  address
})
export const onChangeDescription = (description) => ({
  type: ONCHANGE_DESCRIPTION,
  description
})

export const fetchLocationsBegin = () => ({
  type: FETCH_LOCATIONS_BEGIN
})
export const fetchLocationsSuccess = (location) => ({
  type: FETCH_LOCATIONS_SUCCESS,
  location
})
export const fetchLocationsError = (error) => ({
  type: FETCH_LOCATIONS_ERROR,
  error
})

export const fetchInserLocationBegin = () => ({
  type: FETCH_INSERT_LOCATIONS_BEGIN
})
export const fetchInsertLocationsSuccess = (location) => ({
  type: FETCH_INSERT_LOCATIONS_SUCCESS,
  location
})
export const fetchInsertLocationError = (error) => ({
  type: FETCH_INSERT_LOCATIONS_ERROR,
  error
})

export const fetchUpdateLocationBegin = () => ({
  type: FETCH_UPDATE_LOCATIONS_BEGIN
})
export const fetchUpdateLocationsSuccess = (location) => ({
  type: FETCH_UPDATE_LOCATIONS_SUCCESS,
  location
})
export const fetchUpdateLocationError = (error) => ({
  type: FETCH_UPDATE_LOCATIONS_ERROR,
  error
})

export const fetchDeleteLocationBegin = () => ({
  type: FETCH_DELETE_LOCATIONS_BEGIN
})
export const fetchDeleteLocationsSuccess = (location) => ({
  type: FETCH_DELETE_LOCATIONS_SUCCESS,
  location
})
export const fetchDeleteLocationError = (error) => ({
  type: FETCH_DELETE_LOCATIONS_ERROR,
  error
})

export const fetchLocationSuccess = (id) => ({
  type: SELECT_LOCATION,
  id
})

/*export function selectLocation(id) {
  return dispatch => {
    location =
    dispatch(fetchLocationSuccess(location))
  }
}*/
