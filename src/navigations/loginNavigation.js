import { createStackNavigator } from 'react-navigation';
import loginScreen from './../screens/login';
import registerScreen from './../screens/register';
import locationsScreen from './../screens/locations';
import updateLocation from  './../screens/updateLocation';

const loginStack = createStackNavigator({
  'Login' : {screen: loginScreen},
  'Register': {screen: registerScreen},
  'Locations': {screen: locationsScreen},
  'updateLocation': {screen: updateLocation}
})

export default loginStack;
