import { createDrawerNavigator } from 'react-navigation';
import loginStack from './loginNavigation';


const indexNav = createDrawerNavigator({
  inicio: {screen: loginStack}
})

export default indexNav;
