import React from 'react';
import {View,Text,Image,FlatList} from 'react-native';
import {Input,Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MapView from 'react-native-maps'
/*
*Definicion de componentes para la aplicacion
*/


//Componente para el Titulo de las screens
export const Header = (props) => {
  const {title} = props;
  return (
    <View>
      <Text>{title}</Text>
    </View>
  );
}

//Componente para los imputs de los formularios
export const InputForm = (props) => {
  const {iconName,iconSize,iconColor,placeholder,value,onChangeValue} = props;
  return (
    <View>
      <Input
        leftIcon= {
          <Icon
            name={iconName}
            size={iconSize}
            color={iconColor}
          />
        }
        onChangeText={(val) => onChangeValue(val)}
        placeholder={placeholder}
        value={value} />
    </View>
  );
}


//Componente para crear la tarjeta de location
export const CardLocation = (props) => {
  console.log(props)
  return (
    <View style={{flexDirection:'row', marginBottom: 10}}>
      <View style={{flex: 1, height: 100, backgroundColor:'pink'}}>
        <Image />
      </View>
      <View style={{flex: 2}}>
        <Text>{props.address}</Text>
        <Text>{props.description}</Text>
      </View>
      <View style={{flex: 1, justifyContent:'space-between'}}>
        <View>
          <Button onPress={ () => {
            props.selectLocation(props.id);
            props.navigation.navigate('updateLocation'); }
          } title="Editar"/>
        </View>
        <View>
          <Button onPress={() => {console.log(props); props.deleteLocation(props.ubication)}} title="Eliminar" />
        </View>
      </View>
    </View>
  );
}

//Componente para desplegar lista de ubicaciones

export const ListLocations = (props) => {
  return (
    <FlatList
      data={props.data}
      renderItem={({item}) =>
        <CardLocation
          ubication={item}
          address={item.address}
          description={item.description}
          id={item.id} {...props} />
      }
      keyExtractor={({item},index) => index.toString()}
    />
  );
}

//Componente para cargar el mapa
export const Map = (props) => {
  return (
    <View>
      <MapView
        style={{width:'100%', height: 250}}
        initialRegion={props.initialLocation}
      >

      </MapView>
    </View>
  )
}
