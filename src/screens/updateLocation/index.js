import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Header,InputForm,Map} from '../../components';
import {connect} from 'react-redux';
import {fetchUpdateLocationsSuccess,onChangeAddress,onChangeDescription} from '../../redux/actions/locations';

export class UpdateLocation extends Component {

  constructor(props) {
    super(props)
    this.state = {
      location:null
    }
  }

  componentDidMount() {
    this.setState({
      location: this.props.navigation.getParam('location')
    })
  }

  render() {
    const initialLocation = {
      latitude: 19.78825,
      longitude: -130.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,

    }
    const location = {
      "id": 1,
      "address": "",
      "description": ""
    }
    if (this.props.location) {
      console.log(this.props)
      return (
        <View>
          <Header title='Actualiza tu Ubicacion' />
          <Map />
          <View>
            <InputForm
              iconName='home'
              iconSize={24}
              iconColor='black'
              placeholder='Nombre(casa)'
              onChangeValue={(val) => {this.props.onChangeAddress(val)}}
              value={this.props.location.address} />
            <InputForm
              iconName='home'
              iconSize={24}
              iconColor='black'
              placeholder='Descripcion(calle degollado #115)'
              onChangeValue={(val) => {this.props.onChangeDescription(val)}}
              value={this.props.location.description} />
          </View>
          <View>
            <Button onPress={() => this.props.navigation.navigate('Locations')} title='Cancelar' />
            <Button onPress={() => {
              this.props.updateLocation(this.props.location);
              this.props.navigation.navigate('Locations')}
            } title='Actualizar' />
          </View>
        </View>
      );
    } else {
      return (
        <View>
          <Text>Cargando</Text>
        </View>
      )
    }

  }
}

const mapStateToProps = state => ({
  address: state.LocationReducer.address,
  location: state.LocationReducer.location
})

const mapDispatchToProps = dispatch => ({
  updateLocation: (location) => dispatch(fetchUpdateLocationsSuccess(location)),
  onChangeAddress: (address) => dispatch(onChangeAddress(address)),
  onChangeDescription: (description) => dispatch(onChangeDescription(description)),
})

export default connect(mapStateToProps, mapDispatchToProps)(UpdateLocation)
