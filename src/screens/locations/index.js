import React, {Component} from 'react';
import {View,ScrollView} from 'react-native';
import {Header,ListLocations} from './../../components';
import {connect} from 'react-redux';
import {fetchDeleteLocationsSuccess,fetchLocationSuccess} from '../../redux/actions/locations';
//import {getLocations,insertLocation,DeleteLocation} from '../../redux/actions/locations';

export const Locations = (props) => {
  return (
    <View>
      <Header title="Ubicaciones" />
      <View style={{marginBottom: 5}}>
      <ListLocations data={props.locations} {...props} />
      </View>
    </View>
  );
}

const mapStateToProps = state => ({
  locations: state.LocationReducer.locations,
  location: state.LocationReducer.location
})

const mapDispatchToProps = dispatch => ({
  deleteLocation: (location) => dispatch(fetchDeleteLocationsSuccess(location)),
  selectLocation: (location_id) => dispatch(fetchLocationSuccess(location_id))
})

export default connect(mapStateToProps,mapDispatchToProps)(Locations)
