import React, {Component} from 'react';
import {View} from 'react-native';
import {Input,Button,Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Header,InputForm} from './../../components';
export default class Login extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
      <Header title="Iniciar Sesión" />
        <View>
        <InputForm
          iconName='envelope'
          iconSize={24}
          iconColor='black'
          placeholder='Correo Electronico' />
        <InputForm
          iconName='key'
          iconSize={24}
          iconColor='black'
          placeholder='Contraseña' />
        </View>
        <View>
          <Text onPress={ () => {navigate('Register')} } >¡Registrate!</Text>
        </View>
        <View>
          <Button onPress={ () => {navigate('Locations')} } title='Acceder' />
        </View>
      </View>
    );
  }
}
