import React, {Component} from 'react';
import {View} from 'react-native';
import {Button,Text} from 'react-native-elements';
import {Header,InputForm} from './../../components';
export default class Register extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header title="Registrate" />
        <View>
          <InputForm
            iconName='user'
            iconSize={24}
            iconColor='black'
            placeholder='Nombre' />
          <InputForm
            iconName='envelope'
            iconSize={24}
            iconColor='black'
            placeholder='Correo Electronico' />
          <InputForm
            iconName='key'
            iconSize={24}
            iconColor='black'
            placeholder='Contraseña' />
        </View>
        <View>
          <Text onPress={ () => {navigate('Login')} } >Ya tengo una cuenta</Text>
        </View>
        <View>
          <Button onPress={ () => {navigate('Locations')} } title='Registrate' />
        </View>
      </View>
    );
  }
}
