
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';

export default class indexScreen extends Component {
  render() {

    return (
      <View style={styles.container}>
        <Image  style={styles.image1} source={{uri: 'https://thefact.gr/wp-content/uploads/2018/06/καιροσ-2.jpg'}} />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image1: {
    flex: 1
  },
  image2: {
    flex: 1
  }
});
