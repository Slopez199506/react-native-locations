/** @format */
import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import App from './App';
import {Provider} from 'react-redux';
import store from './src/redux/index';

const redux = () => {
  return (
    <Provider store={store} >
      <App />
    </Provider>
  )
}

AppRegistry.registerComponent(appName, () => redux);
